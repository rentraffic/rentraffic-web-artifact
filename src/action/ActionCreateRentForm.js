import AWS from "aws-sdk";
import { CognitoUserPool } from "amazon-cognito-identity-js";
import {authUser} from '../libs/awsLib'
import config from "../config";
import Validate from '../libs/Validate'

const defaultPayload = ["title", "amount", "per", "deposit", "availability", "description", "image", "tags",];

export function handleSubmit(payload, user_token) {
  let validateForm=[], payload_data={};

  defaultPayload.forEach((i)=> {
    let valid_item = new Validate(i, payload[i]);
    switch (i) {
      case "title" :
        valid_item.required();
        break;
      case "amount" :
        valid_item.required().isNumber();
        break;
      case "deposit" :
        valid_item.required().isNumber();
        break;
      case "availability" :
        valid_item.required().isNumber();
        break;
      case "description" :
        valid_item.required();
        break;
      case "image" :
        valid_item.required();
        break;
      case "tags" :
        valid_item.required();
        break;
    }
    if(valid_item.hasError || valid_item.errorType.length) {
      console.debug(valid_item.key);
      validateForm = validateForm.concat(valid_item.errorMessage);
    } else {
      payload_data[i] = payload[i];
    }

  });

  if(validateForm.length) {
    return async (dispatch)=>dispatch({type:`VALIDATION_ERROR`, form_error: validateForm});
  }
  return async function (dispatch) {

    const url = `${config.apiGateway.URL}/rent/create`;

    dispatch({type:"UPLOADING_VALIDATION", isUploading: true})
    fetch(url, {
      method: 'PUT',
      headers:{
        "Authorization": user_token,
      },
      body: JSON.stringify(payload_data)
    }).then((resp)=>{
      return resp.json()
    })
      .then((resp) => {
        dispatch({type:"UPLOADING_SUCCESS", uploadingHasError: false})
      })
      .catch((err)=> {
        dispatch({type:"UPLOADING_ERROR", uploadingHasError: true, error: err})
      });
  }
}


export function handleItemEntry(name, value) {
  return async function (dispatch) {
    dispatch({type: `NAME_${name.toUpperCase()}`, [name]: value})
  }
}


export function s3Upload(file, i) {
  return async function (dispatch) {
    const key = new Date().getTime();
    dispatch({type: `NAME_IMAGE_UPLOADING`, image_temp: file, key:key})

    var upload = await _s3Upload(file);
    console.debug(upload);
    dispatch({type: `NAME_IMAGE_UPLOAD`, image: upload.Location, key:key})
  }
}
export async function _s3Upload(file) {
    if (!await authUser()) {
      throw new Error("User is not logged in");
    }

    const s3 = new AWS.S3({
      params: {
        region: config.REGION,
        Bucket: config.s3.BUCKET
      }
    });
    const filename = `${AWS.config.credentials
      .identityId}/${Date.now()}-${file.name}`;
    return s3
      .upload({
        Key: filename,
        Body: file,
        ContentType: file.type,
        ACL: "public-read"
      })
      .promise();

}