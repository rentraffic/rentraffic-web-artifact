import React, { Component } from "react";
import {
  CognitoUserPool,
  AuthenticationDetails,
  CognitoUser
} from "amazon-cognito-identity-js";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "../../components/LoaderButton";
import config from "../../config";
import "./Login.css";

export default class ForgotPassword extends Component {
  cognitoUser=null;
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      email: "",
      validationCodeSent: false,
      code: "",
      password: "",
      confirm_password: ""
    };
  }

  login(email, password) {
    const userPool = new CognitoUserPool({
      UserPoolId: config.cognito.USER_POOL_ID,
      ClientId: config.cognito.APP_CLIENT_ID
    });
    const _this = this;
    this.cognitoUser = new CognitoUser({Username: email, Pool: userPool});

    this.cognitoUser.forgotPassword({
      onSuccess: function (result) {
        _this.setState({ isLoading: false, validationCodeSent:true });
      },
      onFailure: function (err) {
        alert(err);
      },
    });
  }

  verifyCodeAndPassword() {
    this.cognitoUser.confirmPassword(this.state.code, this.state.password, this);

  }
  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      await this.login(this.state.email);

    } catch (e) {
      alert(e);
      this.setState({ isLoading: false });
    }
  };

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }
  validateForm() {

    return true;
  }
  render() {
    return (
      <div className="Login">
        {this.state.validationCodeSent ?
          <form onSubmit={this.verifyCodeAndPassword}>
            <FormGroup controlId="code" bsSize="large">
              <ControlLabel>Verification Code</ControlLabel>
              <FormControl
                autoFocus
                type="text"
                value={this.state.code}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="password" bsSize="large">
              <ControlLabel>Password</ControlLabel>
              <FormControl
                autoFocus
                type="password"
                value={this.state.password}
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup controlId="confirm_password" bsSize="large">
              <ControlLabel>Confirm Password</ControlLabel>
              <FormControl
                autoFocus
                type="password"
                value={this.state.confirm_password}
                onChange={this.handleChange}
              />
            </FormGroup>
            <LoaderButton
              block
              bsSize="large"
              disabled={!this.validateForm()}
              type="submit"
              isLoading={this.state.isLoading}
              text="Change Password"
              loadingText="Verifying code and password."
            />
          </form>
          :
          <form onSubmit={this.handleSubmit}>
            <FormGroup controlId="email" bsSize="large">
              <ControlLabel>Email</ControlLabel>
              <FormControl
                autoFocus
                type="email"
                value={this.state.email}
                onChange={this.handleChange}
              />
            </FormGroup>
            <LoaderButton
              block
              bsSize="large"
              disabled={!this.validateForm()}
              type="submit"
              isLoading={this.state.isLoading}
              text="Change Password"
              loadingText="Sending verification code."
            />
          </form>
        }
      </div>
    );
  }
}
