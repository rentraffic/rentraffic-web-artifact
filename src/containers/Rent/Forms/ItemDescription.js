import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import {handleItemEntry} from "../../../action/ActionCreateRentForm";
import {connect} from "react-redux"
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    flex:1
  },
  autoComplete: {
    flex:1
  },
  textField: {
    borderRadius: 2,
    padding:"0 10px",
    border: "1px solid #F1F1F1"
  },
  formControl: {
    margin: theme.spacing.unit,
  },
  menu: {
    width: 200,
  },
});

const EditorComponent = () => <Editor />


class ItemDescription extends Component {

  constructor(props) {
    super(props);

  }

  onEditorStateChange = (editorState) => {
    this.props.dispatch(handleItemEntry("description", draftToHtml(editorState)));

  }

  render() {
    const { classes, CreateRentForm } = this.props;

    return (
      <div className={classes.container}>
        <Editor
          wrapperClassName="demo-wrapper"
          editorClassName={classes.textField}
          onChange={this.onEditorStateChange}
        />
        <textarea
          disabled
          style={{display:"none"}}
          value={CreateRentForm.description}
        />
      </div>
    )
  }
}

ItemDescription.propTypes = {
  classes: PropTypes.object.isRequired,
};
ItemDescription = connect((store) => {
  return {...store}
})(ItemDescription);

export default withStyles(styles)(ItemDescription);