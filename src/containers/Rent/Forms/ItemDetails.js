import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import TagsInput from 'react-tagsinput'
import './items.css'
import { InputLabel } from 'material-ui/Input';
import {handleItemEntry} from "../../../action/ActionCreateRentForm";
import {connect} from "react-redux"

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    flex:1
  },
  autoComplete: {
    flex:1
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
});


class ItemDetails extends React.Component {
  state = {
    tags: [],
    suggestions: [{ id: 3, text: "Philippines" }, { id: 4, text: "America" }]
  };

  handleChange = name => event => {
    this.props.dispatch(handleItemEntry(name, event.target.value));

  };
  handleChangeTag = (tags) => {
    this.props.dispatch(handleItemEntry("tags", tags));
  }
  handleChangeInput = (tag) => {
    this.props.dispatch(handleItemEntry("tag", tag));
  }
  render() {
    const { classes, CreateRentForm } = this.props;
    const { tags, suggestions } = this.state;

    return (
      <form className={classes.container} noValidate autoComplete="off">
        <TextField
          id="full-width"
          label="Item Title"
          InputLabelProps={{
            shrink: true,
          }}
          error={(CreateRentForm.validation.indexOf("title")!==-1)}
          value={CreateRentForm.title}
          onChange={this.handleChange('title')}
          placeholder="Nikon d3400 with various lenses"
          helperText="Short Description of your Item"
          fullWidth
        />
        <div style={{flexDirection:"row", flex: 1}}>
          <InputLabel htmlFor="AutoComplete">Tag your Category</InputLabel>
          <TagsInput
            value={CreateRentForm.tags}
            onChange={this.handleChangeTag}
            addOnBlur={true}
            inputValue={CreateRentForm.tag}
            onChangeInput={this.handleChangeInput}
          />
        </div>
      </form>
    );
  }
}

ItemDetails.propTypes = {
  classes: PropTypes.object.isRequired,
};

ItemDetails = connect((store) => {
  return {...store}
})(ItemDetails);

export default withStyles(styles)(ItemDetails);