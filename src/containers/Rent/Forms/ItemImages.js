import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Dropzone from 'react-dropzone'
import {s3Upload} from "../../../action/ActionCreateRentForm";
import {connect} from "react-redux"
import FileUploadIcon from 'material-ui-icons/FileUpload';
import List, { ListItem, ListItemText } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';
import './items.css'
const styles = theme => {

    return ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
      flex:1
    },
    uploadBox: {
      borderRadius: 4,
      border: "1px dashed",
      borderColor: theme.palette.primary.dark,
      padding: 20,
      textAlign: "center"
    },
    uploadIconBox: {
      color: theme.palette.primary.dark,
      width:60,
      height:60,
      borderRadius: 4,
      border: "1px solid ",
      borderColor: theme.palette.primary.dark
    },
    uploadIconListUploaded: {
      color: theme.palette.primary.dark,
    },
    uploadIconListUploading: {
      color: theme.palette.grey.light,
    },
      circle: {
        animation: "border .7s ease 1 forwards",
        animationIterationCount: "infinite",
    }
  });
}

class ItemImages extends React.Component {
  constructor() {
    super()
    this.state = { files: [] }
  }

  onDrop = (files) => {
    files.forEach((v, i)=> {
      console.debug(v);
      this.uploadFile(v, i)
    });
  }

  uploadFile = async (file, i) =>  {
    if (file && file.size > this.props.config.MAX_ATTACHMENT_SIZE) {
      alert('Please pick a file smaller than 5MB');
      return;
    }

    // this.setState({ isUploading: this.images.length });
    try {
      this.props.dispatch(s3Upload(file, i, this.props.token));
    }
    catch(e) {
      alert(e);
      // this.setState({ isUploading: false });
    }
  }
  listFiles(image_temp, classes) {
    let files = []
      Object.keys(image_temp).forEach((i) => {
        let f = image_temp[i];
        files.push(
          <ListItem button  key={i}>
            <Avatar>
              <FileUploadIcon className={(f.isUploaded) ? classes.uploadIconListUploaded : classes.uploadIconListUploading}/>
            </Avatar>
            <ListItemText primary={f.name} secondary={f.size + " bytes"}/>
            <Divider inset/>
          </ListItem>
        )

      })
    return files;
  }
  render() {
    const { classes, CreateRentForm } = this.props;
    console.debug(CreateRentForm.image_temp.length)
    return (
      <section className={classes.container}>
        <div className="dropzone">
          <Dropzone
            onDrop={this.onDrop}
            className={classes.uploadBox}
            accept="image/*, video/*"
          >
            <Avatar>
              <FileUploadIcon className={classes.circle}/>
            </Avatar>
            <FileUploadIcon className={classes.uploadIconBox}/>
            <p>Try dropping some files here, or click to select files to upload.</p>
          </Dropzone>
        </div>
        <aside className={classes.container}>
          {( !Object.keys(CreateRentForm.image_temp).length ) ? null :
            <List>
              {this.listFiles(CreateRentForm.image_temp, classes)}
            </List>
          }
        </aside>
      </section>
    );
  }
}



ItemImages.propTypes = {
  classes: PropTypes.object.isRequired,
};

ItemImages = connect((store) => {
  return {...store}
})(ItemImages);

export default withStyles(styles)(({ classes, ...props}) => (
  <ItemImages classes={classes} {...props} />
));
