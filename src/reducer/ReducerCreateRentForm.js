const init_profile = {
  title: "",
  amount: 0,
  per: 'hour',
  deposit: 0,
  availability: 0,
  description:"",
  image:[],
  tags:[],
  tag:[],
  image_temp:{},
  validation: []
};


export default function reducer(state=init_profile, action) {
  // Create Validator here!
  switch (action.type) {

    case 'VALIDATION_ERROR':
      return {
        ...state,
        validation:action.form_error
      };
      break;
    case 'VALIDATION_SUCCESS':
      return {
        init_profile
      };
      break;
    case 'NAME_TITLE':
      return {
        ...state,
        ...action
      };
      break;
    case 'NAME_AMOUNT':
      return {
        ...state,
        ...action
      };
      break;
    case 'NAME_DESCRIPTION':
      return {
        ...state,
        ...action
      };
      break;
    case 'NAME_PER':
      return {
        ...state,
        ...action
      };
      break;
    case 'NAME_DEPOSIT':
      return {
        ...state,
        ...action
      };
      break;
    case 'NAME_TAG':
      return {
        ...state,
        ...action
      };
      break;
    case 'NAME_TAGS':
      let tags = action.tags;

      return {
        ...state,
        tags
      };
      break;
    case 'NAME_AVAILABILITY':
      return {
        ...state,
        ...action
      };
      break;
    case 'NAME_IMAGE_UPLOADING':
      let image_temp = state.image_temp;
      let action_temp_image = action.image_temp;

      image_temp[action.key] = action_temp_image;
      image_temp[action.key].isUploaded = false;


      return {
        ...state,
        image_temp
      };
      break;
    case 'NAME_IMAGE_UPLOAD':
      let images_temp = state.image_temp;
      let image = state.image;
      images_temp[action.key].isUploaded = true;

      image.push(action.image);
      return {
        ...state,
        image,
        image_temp: images_temp,

      };
      break;
    default:
      return state;



  }

}
